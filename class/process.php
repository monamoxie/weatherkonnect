<?php   
class __PROCESS__  {
    private $requestLink;
    private $db; 
    public $err;
    public $history = array(); 
    public $temp;
    public $sunset;
    public $sunrise;
    public $retCity;
    public $fileIcon;
    
	
    public function __construct($db)
    {
        $this->db = $db;
		$this->requestLink = 'http://api.openweathermap.org/data/2.5/weather';   
    } 

    public function init($city) 
    {
        $this->city = preg_replace("#[^a-z0-9, ]#i", "", $this->db->cleanData($city));
        // concat extra params
        $this->requestLink = $this->requestLink . '?q='.$city.'&APPID=4c33dd536aacf812d66b4038e677e71c&mode=json&units=metric';
        
        // fetch and log current data
        $this->fetchWeatherData(); 

        // then, Fire up previous history
        $this->weatherHistory();
    }


    public function fetchWeatherData()
    {
        $grab = @file_get_contents($this->requestLink);
        if ($grab === false)
        {
            $this->$err = 'ERROR: No result found';
        } 

       $readFile = json_decode($grab, true);
       if ($readFile)
       {
            //Grab data params
            $this->retCity = $readFile["name"].", ".$readFile["sys"]["country"]; 
            $this->sunrise =date("g:i A",  $readFile["sys"]["sunrise"]);
            $this->sunset = date("g:i A",  $readFile["sys"]["sunset"]);
            $this->temp = round ($readFile["main"]["temp"], 2);
            $this->today = date("l").", ".date("j").date("S")." ".date("F").", ".date("Y");
            $this->fileIcon = $readFile["weather"][0]["icon"].'.png';

                if (!$this->db->runQuery("INSERT INTO searches (city, temp, sunrise, sunset, date) VALUES('$this->retCity', 
                    '$this->temp', '$this->sunrise', '$this->sunset', now() )" ))
                    {
                        $this->err =  'ERROR: An error occured while saving your search history. Please try again';
                }        
        } 
     

    }
    
   
    public function weatherHistory()
    {  
        $this->db->runQuery("SELECT * FROM searches WHERE city = '$this->retCity' ORDER BY id DESC LIMIT 0,3");
        if ($this->db->numRows() > 0){
            while($row = $this->db->getData())
            {
               $this->history[] = $row;
            }	 	  	
        } 
    }    

   
}