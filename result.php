<?php
require_once("class/db.php");
require_once("class/process.php");
$db = new __DB__();
$process = new __PROCESS__($db); 

  if (isset($_GET["city"]))
  {
    $process->init($_GET["city"]);
  }
  else 
  {
    header("location: ".URL);
    exit();
  }	 
						
?>
<!DOCTYPE html>
<html>
  <head>
    <title> Weather Konnect ::: View</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,700,300,200' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'> 
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Weather Konnect" /> 
  </head>
  <body>
    <div class="overlay">
      <h1> Weather Konnect</h1>
      <div class="wrap-left">

        <div class="top-info"> 
          <h3> <?php echo $process->retCity; ?> </h3> <?php echo  $process->err; ?>
          <ul class="etal"> 
            <li> Sunrise at <span><?php echo  $process->sunrise; ?></span> </li>
            <li class="temp" style="text-align:center"> 
            <?php echo '<img src="http://openweathermap.org/img/w/'.$process->fileIcon.'"><span class="temp-wru"> <b>'.$process->temp.'°C </b></span>' ?> 
            </li>
            <li> Sunset at <span> <?php echo $process->sunset;  ?></span> </li>
            <div style="clear:both"> </div>
          </ul> 
        </div>
      
        <div class="bottom-info">
          <h4> <?php echo $process->today; ?> </h4>
        </div>  
      </div>

      <div class="wrap-right">
        <div class="top-info">
          <h3 style="font-size:19px"> SEARCH HISTORY FOR THIS CITY </h3> <hr/>
          <ul class="dew">
            <?php  
              if (count($process->history) < 1) {
                echo '<li class="center p-20"> 
                <strong>Oooop!</strong> No previous search for this city was found.<br/> <br/> 
                <small> But today\'s record has just been stored.  Refresh this page to view this history </small>
                </li>';
              }
              else {
                foreach ($process->history as  $row) {
                  echo ' 
                  <li>
                    <div class="prev-date">
                      <h6 style=""> '.date( 'l', strtotime($row["date"]) ).' </h6>
                      <h6 style=""> '.date( 'jS F, Y', strtotime($row["date"]) ).' </h6>
                      <h6 style=""> '.date( 'g:i A', strtotime($row["date"]) ).' </h6>
                    </div> 
                    
                    <div class="prev-info"> 
                      <h5> Temperature was at <span class="prev-temp"> '.$row["temp"].'°C </span> </h5> 
                        <span class="prev-set"> Sunrise: <i class="dx"> '.$row["sunrise"].' </i> </span> 
                        <span class="prev-set"> Sunset  <i class="dx"> '.$row["sunset"].' </i> </span>
                    </div>   
                        
                  </li>';
                }
              }
                
            ?>
            <div style="clear:both"> </div>
          </ul> 
        </div>

        <div class="bottom-info">
          <a href="<?php echo URL ?>"><h4> Back to Home</h4></a>
        </div>
      </div>
      <div style="clear:both"> </div> 
    </div>
  </body>
</html>